# Language Detection Endpoint

## About this endpoint
This endpoint is useful for finding out which all languages are present in your endpoint

## Getting started
**Python code**
``` python
# make sure you have python3 and pip3 installed on your computer
# make sure you have requests library installed (pip3 install requests)
# get your api key from rapidapi console

import json
import requests


api_key = '***<insert your api key>***'


def get_result(text: str, api_key: str):
    url = 'https://text-analysis12.p.rapidapi.com/language-detection/api/v1.1'
    params = {
        'text': text
    }
    headers = {
        'x-rapidapi-key': api_key,
        'x-rapidapi-host': 'text-analysis12.p.rapidapi.com',
        'content-type': 'application/json'
    }

    try:
        response = requests.post(url=url, headers=headers, json=params)
        result = response.json()

        if result.get('ok', False):
            language_probability = result['language_probability']
            return json.dumps(language_probability, indent=2)
        else:
            print('error:', result.get('msg'))

    except Exception as e:
        print(f'error: {e}')
    return {}


print(get_result('Have you heard about The Guardian?', api_key))
print(get_result('क्या आपने दैनिक भास्कर के बारे में हिंदी में सुना है?', api_key))
print(get_result('هل سمعت عن الجزيرة', api_key))
```

## Languages supported
**full-code, short-code**
- english, en
- hindi, hi
- arabic, ar

## Service Limits
- rate limit: 100 requests per second
- max character in text field: 100000 chars

## Other resources
- Language code: [Follows ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)

